from transformers import AutoModelWithLMHead, AutoTokenizer
import torch
import json
from tqdm import tqdm

tokenizer = AutoTokenizer.from_pretrained("tuner007/t5_abs_qa")
model = AutoModelWithLMHead.from_pretrained("tuner007/t5_abs_qa")
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = model.to(device)


def get_answer(question, context):
    input_text = "context: %s <question for context: %s </s>" % (context,question)
    features = tokenizer([input_text], return_tensors='pt')
    out = model.generate(input_ids=features['input_ids'].to(device), attention_mask=features['attention_mask'].to(device))
    ans =  tokenizer.decode(out[0])
    return ans[6:len(ans)-5]


DATA_DIR = "squad_data/"
#read test dataset
FILE_NAME = "test-qar_squad_metrics_summ.jsonl"
PATH = DATA_DIR+FILE_NAME

FILE_NAME = "abstractive-qa-summ.jsonl"
SAVE_PATH = DATA_DIR+FILE_NAME

DATA = []
with open(PATH, 'r') as fp:

    for line in fp: 

        data_row = json.loads(line)  
        DATA.append(data_row)


OP = []

for row in tqdm(DATA):

    context = row["context"]
    question = row["qas"][0]["question"]
    id = row["qas"][0]["id"]

    ans = get_answer(question, context)
    
    TEMP = {}

    TEMP["id"] = id
    TEMP["generated_ans"] = ans

    OP.append(TEMP)

for row in OP:

    json_object = json.dumps(row)
    with open(SAVE_PATH, "a") as outfile:
        outfile.write(json_object)
        outfile.write('\n')

