from simpletransformers.question_answering import QuestionAnsweringModel, QuestionAnsweringArgs
import sys
import json

USE_CUDA = int(sys.argv[1])

if USE_CUDA == 0:

    USE_CUDA = False

else:
    USE_CUDA = True

print("Loading the best model ...")

model = QuestionAnsweringModel('bert', f'outputs/best_model', use_cuda=USE_CUDA)

#read test data
dev_data = []
with open('squad_data/test_spans_summ.jsonl', 'r') as f:
    
    for line in f: 

        data_row = json.loads(line)  
        dev_data.append(data_row)


print(f"total test_samples - {len(dev_data)}")

preds = model.predict(dev_data)

ans_preds = preds[0] 

SAVE_DIR = "squad_data/"
SAVE_FILE_NAME = f"answers_bert_base_spans_summ.jsonl"
SAVE_PATH = SAVE_DIR + SAVE_FILE_NAME


for row in ans_preds:

    json_object = json.dumps(row)

    with open(SAVE_PATH, "a") as outfile:
        outfile.write(json_object)
        outfile.write('\n')