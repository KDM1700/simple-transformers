from simpletransformers.question_answering import QuestionAnsweringModel, QuestionAnsweringArgs
import sys
import json

USE_CUDA = int(sys.argv[1])

if USE_CUDA == 0:

    USE_CUDA = False

else:

    USE_CUDA = True


train_args = {
    'learning_rate': 3e-5,
    'num_train_epochs': 15,
    'n_best_size': 1,
    'overwrite_output_dir': True,
    'evaluate_during_training': True,
    'save_eval_checkpoints': False,
    'save_model_every_epoch': False,
    'save_steps': -1,
    'train_batch_size': 16,
    'evaluate_during_training_verbose': True,
    'gradient_accumulation_steps': 8,
}

model = QuestionAnsweringModel('bert', 'bert-base-uncased', use_cuda=USE_CUDA, args=train_args)

#model = QuestionAnsweringModel('bert', 'outputs/checkpoint-1780-epoch-10', use_cuda=USE_CUDA, args=train_args)
train_data = []
#read training data
with open('squad_data/train_spans_summ.jsonl', 'r') as fp:

    for line in fp: 

        data_row = json.loads(line)   
        train_data.append(data_row)

print(f"Total train samples - {len(train_data)}")

val_data = []
#read validation data
with open('squad_data/val_spans_summ.jsonl', 'r') as fp:

    for line in fp: 

        data_row = json.loads(line)  
        val_data.append(data_row)

print(f"Total val samples - {len(val_data)}")

#start training
model.train_model(train_data, eval_data = val_data)


