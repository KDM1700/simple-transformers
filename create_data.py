import json
from tqdm import tqdm
import sys

NUM_SAMPLES = int(sys.argv[1])

DATA_DIR = "../data/"
FILE_NAME = "test-qar_squad_all.jsonl"
PATH = DATA_DIR+FILE_NAME

SAVE_DIR = "../drive/"
SAVE_FILE_NAME = f"bert_data-{NUM_SAMPLES}.jsonl"
SAVE_PATH = SAVE_DIR + SAVE_FILE_NAME

#read data

DATA = []
with open(PATH, 'r') as fp:

    for line in fp: 
        data_row = json.loads(line) 
        if len(data_row["qas"][0]["answers"]) != 0:
            
            DATA.append(data_row)

print(f"Total data samples - {len(DATA)}")


DATA = DATA[:5000]

OP = []

for i in tqdm(range(len(DATA))):

    data_row = DATA[i]
    context = data_row["context"]
    question = data_row["qas"][0]["question"]
    id = data_row["qas"][0]["id"]
    is_impossible = data_row["qas"][0]["is_impossible"]

    ansList_bleu2 = data_row["qas"][0]["answers_snippet_spans_bleu2"] 
    ansList_bleu4 = data_row["qas"][0]["answers_snippet_spans_bleu4"] 
    ansList_sentence_bleu2 = data_row["qas"][0]["answers_sentence_bleu2"] 
    ansList_sentence_bleu4 = data_row["qas"][0]["answers_sentence_bleu4"] 
    human_answers = data_row["qas"][0]["human_answers"]

    QAS = [{'id':id, 'is_impossible': is_impossible, 'question': question, 'answers':ansList_bleu2}]

   
    TEMP = {}
    TEMP["context"] = context
    TEMP["qas"] = QAS
    OP.append(TEMP)

for row in OP:

    json_object = json.dumps(row)
    with open(SAVE_PATH, "a") as outfile:
        outfile.write(json_object)
        outfile.write('\n')
