import sys
import json
import numpy as np

#read test data
dev_data = []
with open('squad_data/test-qar_squad_short_context_cleaned.jsonl', 'r') as f:
    
    for line in f: 

        data_row = json.loads(line)  
        dev_data.append(data_row)

print(len(dev_data))

dev_data = dev_data[:5000]
#read preds file
preds_data = []
with open('squad_data/answers_short_context_bert_large.jsonl', 'r') as f:
    
    for line in f: 

        data_row = json.loads(line)  
        preds_data.append(data_row)



OP = []


len_empty = []
len_non_empty = []

for i in range(len(dev_data)):
    
    row_preds = preds_data[i]
    data_row = dev_data[i]

    context = data_row["context"]
    question = data_row["qas"][0]["question"]
    id_dev = data_row["qas"][0]["id"]

    ansList_bleu2 = data_row["qas"][0]["answers_snippet_spans_bleu2"] 
    ansList_bleu4 = data_row["qas"][0]["answers_snippet_spans_bleu4"] 
    ansList_sentence_bleu2 = data_row["qas"][0]["answers_sentence_bleu2"] 
    ansList_sentence_bleu4 = data_row["qas"][0]["answers_sentence_bleu4"] 
    human_answers = data_row["qas"][0]["human_answers"]


    answers_bleu2 = []
    answers_bleu4 = []
    answers_sentence_bleu2 = []
    answers_sentence_bleu4 = []

    for answerObj in ansList_bleu2:
        answers_bleu2.append(answerObj['text'])

    for answerObj in ansList_bleu4:
        answers_bleu4.append(answerObj['text'])

    for answerObj in ansList_sentence_bleu2:
        answers_sentence_bleu2.append(answerObj['text'])

    for answerObj in ansList_sentence_bleu4:
        answers_sentence_bleu4.append(answerObj['text'])
    
    ans = row_preds["answer"][0]
    id_preds = row_preds["id"]

    len_context = len(context.split())

    assert id_dev == id_preds

    if ans != "":
    

        TEMP = {}
        TEMP["question"] = question
        TEMP["answers_sentence_bleu2"] = answers_sentence_bleu2
        TEMP["answers_sentence_bleu4"] = answers_sentence_bleu4
        TEMP["answers_bleu2"] = answers_bleu2
        TEMP["answers_bleu4"] = answers_bleu4
        TEMP["answers_human"] = human_answers 
        TEMP["generated_ans"] = ans
        OP.append(TEMP)


SAVE_DIR = "squad_data/"
SAVE_FILE_NAME = f"eval_file_short_context_bert_large_empty.jsonl"
SAVE_PATH = SAVE_DIR + SAVE_FILE_NAME


for row in OP:

    json_object = json.dumps(row)

    with open(SAVE_PATH, "a") as outfile:
        outfile.write(json_object)
        outfile.write('\n')
