#obtain F1 and BLEU scores
import json
import sys
from tqdm import tqdm
from nltk.translate.bleu_score import SmoothingFunction
import nltk

DATA_DIR = "squad_data/"
FILE_NAME = "eval_file_short_context_bert_large_empty.jsonl"
PATH = DATA_DIR+FILE_NAME

DATA = []

with open(PATH, 'r') as fp:

    for line in fp: 

        data_row = json.loads(line)  
        DATA.append(data_row)

print(f"Total data samples - {len(DATA)}")



def normalize_text(s):
    """Removing articles and punctuation, and standardizing whitespace are all typical text processing steps."""
    import string, re

    def remove_articles(text):
        regex = re.compile(r"\b(a|an|the)\b", re.UNICODE)
        return re.sub(regex, " ", text)

    def white_space_fix(text):
        return " ".join(text.split())

    def remove_punc(text):
        exclude = set(string.punctuation)
        return "".join(ch for ch in text if ch not in exclude)

    def lower(text):
        return text.lower()

    return white_space_fix(remove_articles(remove_punc(lower(s))))

def compute_exact_match(prediction, truth):
    return int(normalize_text(prediction) == normalize_text(truth))

def calculate_bleu(gold_answers, generated_answers):

    scores = [0,0,0,0]
    gen_ans = generated_answers
    gen_ans = normalize_text(gen_ans)
    hypothesis = gen_ans.split()

    references = []

    for gold_ans in gold_answers:

        ans = normalize_text(gold_ans)
        ref  = ans.split()
        references.append(ref)

    chencherry = SmoothingFunction()

    
    if len(references) > 0:
            
    
        BLEU_1 = nltk.translate.bleu_score.sentence_bleu(references, hypothesis, weights=(1,0,0,0), smoothing_function=chencherry.method1)
        BLEU_2 = nltk.translate.bleu_score.sentence_bleu(references, hypothesis, weights=(1.0/2.0,1.0/2.0),smoothing_function=chencherry.method1)
        BLEU_3 = nltk.translate.bleu_score.sentence_bleu(references, hypothesis, weights=(1.0/3.0,1.0/3.0,1.0/3.0), smoothing_function=chencherry.method1)
        BLEU_4 = nltk.translate.bleu_score.sentence_bleu(references, hypothesis, smoothing_function=chencherry.method1)


        scores = [BLEU_1, BLEU_2, BLEU_3, BLEU_4]

    return scores

def getAverageBleuScores(BLEU_SCORES):

    BLEU_AVGS = [0,0,0,0]

    for score in BLEU_SCORES:

        BLEU_AVGS[0] += score[0]
        BLEU_AVGS[1] += score[1]
        BLEU_AVGS[2] += score[2]
        BLEU_AVGS[3] += score[3]

    BLEU_FINAL = []

    for score in BLEU_AVGS:

        BLEU_FINAL.append(score/len(DATA))

    return BLEU_FINAL




def cal_score_f1(gold_ans, gen_ans):

    gold_ans = normalize_text(gold_ans)
    gen_ans = normalize_text(gen_ans)

    pred_tokens = gen_ans.split()
    truth_tokens = gold_ans.split()
    # if either the prediction or the truth is no-answer then f1 = 1 if they agree, 0 otherwise
    if len(pred_tokens) == 0 or len(truth_tokens) == 0:
    
        return int(pred_tokens == truth_tokens)
    
    common_tokens = set(pred_tokens) & set(truth_tokens)

    # if there are no common tokens then f1 = 0
    if len(common_tokens) == 0:
            
        return 0

    prec = len(common_tokens) / len(pred_tokens)
    rec = len(common_tokens) / len(truth_tokens)
    return 2 * (prec * rec) / (prec + rec)


def compute_f1(gold_answers, generated_answers):

    num_samples = len(gold_answers)
    score = 0

    for i in range(num_samples):

        prediction = generated_answers
        truth = gold_answers[i]

    
        curr_score = cal_score_f1(truth, prediction)
        score = max(score,curr_score)

    return score


avg_score_human = 0
avg_score_bleu2 = 0
avg_score_bleu4 = 0
avg_score_sentences_bleu2 = 0
avg_score_sentences_bleu4 = 0

BLEU_SCORES_HUMAN = []
BLEU_SCORES_ANSWERS_BLEU2 = []
BLEU_SCORES_ANSWERS_BLEU4 = []
BLEU_SCORES_ANSWERS_SENTENCES_BLEU2 = []
BLEU_SCORES_ANSWERS_SENTENCES_BLEU4 = []

for i in tqdm(range(len(DATA))):

    data_row = DATA[i]

    answers_sentence_bleu2 =   data_row["answers_sentence_bleu2"]
    answers_sentence_bleu4 = data_row["answers_sentence_bleu4"]
    answers_bleu2 = data_row["answers_bleu2"]
    answers_bleu4 = data_row["answers_bleu4"] 
    human_answers = data_row["answers_human"] 
    generated_answers = data_row["generated_ans"] 

    bleu_scores = calculate_bleu(human_answers, generated_answers)
    BLEU_SCORES_HUMAN.append(bleu_scores)

    bleu_scores = calculate_bleu(answers_bleu2, generated_answers)
    BLEU_SCORES_ANSWERS_BLEU2.append(bleu_scores)

    bleu_scores = calculate_bleu(answers_bleu4, generated_answers)
    BLEU_SCORES_ANSWERS_BLEU4.append(bleu_scores)

    bleu_scores = calculate_bleu(answers_sentence_bleu2, generated_answers)
    BLEU_SCORES_ANSWERS_SENTENCES_BLEU2.append(bleu_scores)

    bleu_scores = calculate_bleu(answers_sentence_bleu4, generated_answers)
    BLEU_SCORES_ANSWERS_SENTENCES_BLEU4.append(bleu_scores)

    sc = compute_f1(human_answers, generated_answers)
    avg_score_human += sc

    sc = compute_f1(answers_bleu2, generated_answers)
    avg_score_bleu2 += sc

    sc = compute_f1(answers_bleu4, generated_answers)
    avg_score_bleu4 += sc

    sc = compute_f1(answers_sentence_bleu2, generated_answers)
    avg_score_sentences_bleu2 += sc

    sc = compute_f1(answers_sentence_bleu4, generated_answers)
    avg_score_sentences_bleu4 += sc

print(f"F1 score of human_ans vs generated ans -- {avg_score_human/len(DATA)}")
print(f"F1 score of bleu2_ans vs generated ans -- {avg_score_bleu2/len(DATA)}")
print(f"F1 score of bleu4_ans vs generated ans -- {avg_score_bleu4/len(DATA)}")
print(f"F1 score of bleu2_ans_sentences vs generated ans -- {avg_score_sentences_bleu2/len(DATA)}")
print(f"F1 score of bleu4_ans_sentences vs generated ans -- {avg_score_sentences_bleu4/len(DATA)}")



print(f"BLEU scores of human_ans vs generated ans -- {getAverageBleuScores(BLEU_SCORES_HUMAN)}")
print(f"BLEU scores of bleu2_ans vs generated ans -- {getAverageBleuScores(BLEU_SCORES_ANSWERS_BLEU2)}")
print(f"BLEU scores of bleu4_ans vs generated ans -- {getAverageBleuScores(BLEU_SCORES_ANSWERS_BLEU4)}")
print(f"BLEU scores of bleu2_ans_sentences vs generated ans -- {getAverageBleuScores(BLEU_SCORES_ANSWERS_SENTENCES_BLEU2)}")
print(f"BLEU scores of bleu4_ans_sentences vs generated ans -- {getAverageBleuScores(BLEU_SCORES_ANSWERS_SENTENCES_BLEU4)}")

